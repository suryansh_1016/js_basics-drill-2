// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function CarDetails(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Array not found");
    return;
  } else {
    const carId = 33;
    const result = inventory.find(function (car) {
      return car.id == carId;
    });

    if (result) {
      console.log(
        `Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`
      );
    } else {
      console.log("Car Id not found");
    }
  }
}

module.exports = CarDetails;

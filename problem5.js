// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
function returnOldCars(inventory, yearData) {
  if (!Array.isArray(inventory)) {
    console.log("Array not found");
    return;
  }

  if (inventory.length == 0) {
    console.log("Array is empty");
    return;
  } else {
    const oldCars = inventory
      .map((car, index) => ({ car, year: yearData[index] }))
      .filter(({ year }) => year < 2000)
      .map(({ car }) => car);

    console.log(oldCars, oldCars.length);
  }
}

module.exports = returnOldCars;

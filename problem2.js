// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"

function returnLastCar(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Array not found");
    return;
  }

  const lastCar = inventory.reduce(function (acc, car) {
    return car;
  }, null);

  if (lastCar) {
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
  } else {
    console.log("Inventory is empty");
  }
}

module.exports = returnLastCar;

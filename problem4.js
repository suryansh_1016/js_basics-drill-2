// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function returnCarYears(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Array not found");
    return;
  }

  if (inventory.length == 0) {
    console.log("Array is empty");
  } else {
    const CarModels = inventory.map(function (car) {
      return car.car_year;
    });
    return CarModels;
  }
}

module.exports = returnCarYears;
